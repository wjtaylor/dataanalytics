\name{loans}
\alias{loans}
\docType{data}
\title{
Data on Consumer Loans
}
\description{
This dataset classifies people described by a set of attributes as good or bad credit risks.  This data was generated in Germany, hence all currency values are in Deutsche Marks (DM).
}
\usage{data(loans)}
\format{
  A data frame with 1000 observations on the following 21 variables.
  \describe{
    \item{\code{Status.of.existing.checking.account}}{ 
      
      \code{... < 0 DM}
      
      \code{0 <= ... < 200 DM}
      
      \code{... >= 200 DM / salary assignments for at least 1 year} 
      
      \code{no checking account}}
    \item{\code{Duration.in.month}}{Length of loan}
    \item{\code{Credit.history}}{
        
        \code{all credits at this bank paid back duly} 
        
        \code{critical account/other credits existing (not at this bank)} 
        
        \code{delay in paying off in the past} 
        
        \code{existing credits paid back duly till now} 
        
        \code{no credits taken/all credits paid back duly}}
    \item{\code{Purpose}}{
        
        \code{business} 
        
        \code{car (new)} 
        
        \code{car (used)} 
        
        \code{domestic appliances} 
        
        \code{education} \code{furniture/equipment} 
        
        \code{others} \code{radio/television} 
        
        \code{repairs} 
        
        \code{retraining}}
    \item{\code{Credit.amount}}{Amount borrowed in DM}
    \item{\code{Savings.account.bonds}}{ 
    
        \code{.. >= 1000 DM} 
        
        \code{... < 100 DM} 
        
        \code{100 <= ... < 500 DM} 
        
        \code{500 <= ... < 1000 DM} 
        
        \code{unknown/ no savings account}}
    \item{\code{Present.employment.since}}{ 
        
        \code{.. >= 7 years} 
        
        \code{... < 1 year} 
        
        \code{1 <= ... < 4 years} 
        
        \code{4 <= ... < 7 years} 
        
        \code{unemployed}}
    \item{\code{Installment.rate.in.percentage.of.disposable.income}}{Loan installment rate}
    \item{\code{Personal.status.and.sex}}{
    
        \code{female : divorced/separated/married} 
        
        \code{male : divorced/separated} 
        
        \code{male : married/widowed} 
        
        \code{male : single}}
    \item{\code{Other.debtors.guarantors}}{ 
        
        \code{co-applicant} 
        
        \code{guarantor} 
        
        \code{none}}
    \item{\code{Present.residence.since}}{Years at current residence}
    \item{\code{Property}}{
        \code{real estate} 
    
        \code{building society savings agreement/life insurance} 
        
        \code{car or other} 
        
        \code{unknown / no property}}
    \item{\code{Age.in.years}}{Age of applicant}
    \item{\code{Other.installment.plans}}{
    
        \code{bank} 
        
        \code{none} 
        
        \code{stores}}
    \item{\code{Housing}}{ 
    
        \code{for free} 
        
        \code{own} 
        
        \code{rent}}
    \item{\code{Number.of.existing.credits.at.this.bank}}{Existing credits}
    \item{\code{Job}}{
    
        \code{management/ self-employed/highly qualified employee/ officer} 
        
        \code{skilled employee / official} 
        
        \code{unemployed/ unskilled - non-resident} 
        
        \code{unskilled - resident}}
    \item{\code{Number.of.people.being.liable.to.provide.maintenance.for}}{a numeric vector}
    \item{\code{Telephone}}{
    
        \code{none} 
        
        \code{yes, registered under the customers name}}
    \item{\code{foreign.worker}}{
    
        \code{no} 
        
        \code{yes}}
    \item{\code{Good.Loan}}{1 = Good (paid off) and 2 = Bad (defaulted)}
  }
}
\source{
UCI Machine Learning Repository: \emph{http://archive.ics.uci.edu/ml/datasets/Statlog+(German+Credit+Data)}
}
\references{
Zumel, N. and Mount, J. (2013) \emph{Practical Data Science with R}. Manning Publications.
}
\examples{
data(loans)

#Default levels by type of loan
loantype<-table(loans$Purpose,loans$Good.Loan)
round(prop.table(loantype,1),2)
}
\keyword{datasets}
