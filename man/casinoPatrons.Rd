\name{casinoPatrons}
\alias{casinoPatrons}
\docType{data}
\title{
Casino Patron Data
}
\description{
Fictitious individual gambler information.  Complements the \code{casinoTrips} data frame.
}
\usage{data(casinoPatrons)}
\format{
  A data frame with 10000 observations on the following 6 variables.
  \describe{
    \item{\code{ID}}{Primary key to identify casino patrons}
    \item{\code{Age}}{Age at time of card signup}
    \item{\code{Sex}}{M for male, F for female}
    \item{\code{HasTel}}{Dummy code for whether the patron has a telephone number (for marketing purposes)}
    \item{\code{MailTo}}{Mailing status of the guest.  \emph{Mail} means a valid mailing address.  \emph{Bad} means an invalid mailing address. \emph{No Mail} means that the guest does not want to be contacted through direct mail.}
    \item{\code{Zip}}{Zip code}
  }
}
\examples{
data(casinoPatrons)

#Age distribution
hist(casinoPatrons$Age,breaks=50,main="Age Distribution",xlab="Age in Years")

#Cross-tabulation of marketing groups
table(casinoPatrons$MailTo,casinoPatrons$HasTel)

}
\keyword{datasets}
